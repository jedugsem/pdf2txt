//extern crate pdf_extract;
//extern crate lopdf;

//use std::env;
use lopdf::*;
use pdf_extract::*;
use std::fs::File;
use std::io::BufWriter;
use std::path;
use std::path::PathBuf;

fn main() {
    //let output_kind = "html";
    let output_kind = "txt";
    //let output_kind = "svg";
    //let file = env::args().nth(1).unwrap();
    let files = vec![
        "FG_1_Prueffragen_mL_Februar_2021.pdf",
        "FG_2_Prueffragen_mL_Februar_2021.pdf",
        "FG_3_Prueffragen_mL_Februar_2021.pdf",
        "FG_4_Prueffragen_mL_Februar_2021.pdf",
        "FG_5_Prueffragen_mL_Februar_2021.pdf",
    ];
    for i in files {
        println!("{}", i);
        let file = format!("/home/me/jedugsem/jagd/data/2021mitLosung/{}",i);
        let path = path::Path::new(&file);
        let filename = path.file_name().expect("expected a filename");

        let mut output_file = PathBuf::new();
        output_file.push("/home/me/jedugsem/jagd/data/text");
        output_file.push(filename);
        output_file.set_extension(&output_kind);

        let mut output_file =
            BufWriter::new(File::create(output_file).expect("could not create output"));
        
        let doc = Document::load(path).unwrap();
        print_metadata(&doc);

        let mut output: Box<dyn OutputDev> = Box::new(PlainTextOutput::new(
            &mut output_file as &mut dyn std::io::Write,
        ));
        println!("loaded");

        println!("{:?}",output_doc(&doc, output.as_mut()));
    }
}
